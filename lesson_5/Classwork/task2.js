/*

    Задание 2.

    Напишите фунцию, которая изменяет цвет-фона и цвет текста, присваивая к новым цветам
    значение из this.color, this.background
    А так же добавляет элемент h1 с текстом "I know how binding works in JS"

    1.1 Ф-я принимает один аргумент,
    второй попадает в него через метод .call(obj)

    1.2 Ф-я не принимает никаких аргументов,
    а необходимые настройки полностью передаются через bind

    1.3 Ф-я принимает фразу для заголовка,
    обьект с настройками передаем через .apply();

*/
document.addEventListener('DOMContentLoaded', function() {
  let colors = {
    background: 'purple',
    color: 'white'
  }

  function myCall( back ){
   let h1 = document.createElement('h1');
    document.body.appendChild(h1);
    h1.innerHTML = 'I know how binding works in JS'
    document.body.style.background = back;
    document.body.style.color = this.color;
  }
  
   myCall.call(colors,'green');

  function myCall2( ){
    let h1 = document.createElement('h1');
    document.body.appendChild(h1);
    h1.innerHTML = 'I know how binding works in JS'
    document.body.style.background = this.background;
    document.body.style.color = this.color;
  }
   let myCall3 = myCall2.bind( colors );
   //myCall3();
 
let title = ['I know how binding works in JS'];

function myCall4 ( title ) {
    let h1 = document.createElement('h1');
    document.body.appendChild(h1);
    h1.innerHTML = title;
    document.body.style.background = this.background;
    document.body.style.color = this.color;
}

//myCall4.apply(colors,title);






}) // domcongtentloaded 

